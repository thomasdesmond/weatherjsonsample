﻿using System;
using System.Collections.Generic;
using WeatherJsonSample.Models;
using Xamarin.Forms;

namespace WeatherJsonSample
{
    public partial class MoreInfoPage : ContentPage
    {
        public MoreInfoPage()
        {
            InitializeComponent();
        }

        public MoreInfoPage(DailyData dailyData)
        {
            InitializeComponent();

            BindingContext = dailyData;
        }
    }
}
