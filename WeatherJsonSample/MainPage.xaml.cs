﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WeatherJsonSample.Models;
using Xamarin.Forms;

namespace WeatherJsonSample
{
    public partial class MainPage : ContentPage
    {
        WeatherData weatherDataFromJson = new WeatherData();

        public MainPage()
        {
            InitializeComponent();

            ReadInJsonFile();
        }

        private void ReadInJsonFile()
        {
            var fileName = "WeatherJsonSample.weather.json";

            var assembly = typeof(MainPage).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream(fileName);

            WeatherData myData;
            using (var reader = new System.IO.StreamReader(stream))
            {
                var jsonAsString = reader.ReadToEnd();
                weatherDataFromJson = JsonConvert.DeserializeObject<WeatherData>(jsonAsString);
            }


            WeatherListView.ItemsSource = new ObservableCollection<DailyData>(weatherDataFromJson.Daily.SingleDaysWeather);
            //locationLabel.Text = weatherDataFromJson.Location;
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var itemClicked = menuItem.CommandParameter as DailyData;
            await Navigation.PushAsync(new MoreInfoPage(itemClicked));
        }
    }
}
